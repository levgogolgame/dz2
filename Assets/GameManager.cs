﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	enum units { ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE
	};
	enum eleven_ninteen { ELEVEN, TWELVE, THIRTEEN, FOURTEEN, FIFTEEN, SIXTEEN, SEVENTEEN, EIGHTEEN, NINETEEN
	};
	enum tens { TEN, TWENTY, THIRTY, FORTY, FIFTY, SIXTY, SEVENTY, EIGHTY, NINETY,
	};
	
	enum Msg{ ONE, TWO, THREE, FOUR, UNITS, ELEVEN_NINETEEN, VOID}
	
	[SerializeField] private InputField _inputField;
	[SerializeField] private Text _output;
	private List<string> unitsList;
	private List<string> eleven_ninteenList;
	private List<string> tensList;
	private bool hasError;

	void Start() {
		unitsList = Enum.GetValues(typeof(units)).Cast<units>().Select(v => v.ToString()).ToList();
		eleven_ninteenList = Enum.GetValues(typeof(eleven_ninteen)).Cast<eleven_ninteen>().Select(v => v.ToString()).ToList();
		tensList = Enum.GetValues(typeof(tens)).Cast<tens>().Select(v => v.ToString()).ToList();
	}

	private void Update() {
		if (Input.GetButtonDown("Submit")) {
			Submit();
		}
	}

	public void Submit() {
		TranslateToNumbers(GetInput().ToUpper());
	}

	private void TranslateToNumbers(string input) {
		string[] words = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
		int result = -1;
		hasError = false;

		if (words.Length > 0) {
			if (unitsList.Contains(words[0])) {
				result = GetUnits(words[0]);

				if (words.Length > 1) {
					if (words[1].Equals("HUNDRED")) {
						result *= 100;
						
						if (words.Length > 2) {
							if (unitsList.Contains(words[2])) {
								result += GetUnits(words[2]);

								if (words.Length > 3) {
									ErrorMsg(Msg.UNITS);
								}
							} else if (eleven_ninteenList.Contains(words[2])) {
								result += GetEleven_Nineteen(words[2]);

								if (words.Length > 3) {
									ErrorMsg(Msg.ELEVEN_NINETEEN);
								}
							} else if (tensList.Contains(words[2])) {
								result += GetTens(words[2]);

								if (words.Length > 3) {
									if (unitsList.Contains(words[3])) {
										result += GetUnits(words[3]);

										if (words.Length > 4) {
											ErrorMsg(Msg.UNITS);
										}
									} else {
										ErrorMsg(Msg.FOUR);
									}
								}
							} else {
								ErrorMsg(Msg.THREE);
							}
						}
					} else {
						ErrorMsg(Msg.TWO);
					}
				}
			} else if (tensList.Contains(words[0])) {
				result = GetTens(words[0]);

				if (words.Length > 1) {
					if (unitsList.Contains(words[1])) {
						result += GetUnits(words[1]);

						if (words.Length > 2) {
							ErrorMsg(Msg.UNITS);
						}
					} else {
						ErrorMsg(Msg.TWO);
					}
				}
			} else if (eleven_ninteenList.Contains(words[0])) {
				result = GetEleven_Nineteen(words[0]);

				if (words.Length > 1) {
					ErrorMsg(Msg.ELEVEN_NINETEEN);
				}
			} else {
				ErrorMsg(Msg.ONE);
			}
		} else {
			ErrorMsg(Msg.VOID);
		}

		if (!hasError) {
			_output.text = result.ToString();
			ArabicToRoman(result);
		}
	}

	private void ArabicToRoman(int result) {
		string resultR = "";

		switch (result / 100) {
			case 1: resultR += "C"; break;
			case 2: resultR += "CC"; break;
			case 3: resultR += "CCC"; break;
			case 4: resultR += "CD"; break;
			case 5: resultR += "D"; break;
			case 6: resultR += "DC"; break;
			case 7: resultR += "DCC"; break;
			case 8: resultR += "DCCC"; break;
			case 9: resultR += "CM"; break;			
		}		
		switch (result / 10 % 10) {
			case 1: resultR += "X"; break;
			case 2: resultR += "XX"; break;
			case 3: resultR += "XXX"; break;
			case 4: resultR += "XL"; break;
			case 5: resultR += "L"; break;
			case 6: resultR += "LX"; break;
			case 7: resultR += "LXX"; break;
			case 8: resultR += "LXXX"; break;
			case 9: resultR += "XC"; break;			
		}	
		switch (result % 10) {
			case 1: resultR += "I"; break;
			case 2: resultR += "II"; break;
			case 3: resultR += "III"; break;
			case 4: resultR += "IV"; break;
			case 5: resultR += "V"; break;
			case 6: resultR += "VI"; break;
			case 7: resultR += "VII"; break;
			case 8: resultR += "VIII"; break;
			case 9: resultR += "IX"; break;			
		}

		_output.text += "\n" + resultR;

	}

	private void ErrorMsg(Msg m) {
		hasError = true;
		_output.color = Color.red;
		
		switch (m) {
			case Msg.ONE: {
				_output.text = "Ошибка в первом слове";
				break;
			}
			case Msg.TWO: {
				_output.text = "Ошибка во втором слове"; 
				break;
			}
			case Msg.THREE: {
				_output.text = "Ошибка в третьем слове"; 
				break;
			}
			case Msg.FOUR: {
				_output.text = "Ошибка в четвёртом слове"; 
				break;
			}
			case Msg.VOID: {
				_output.text = "Ошибка. Пустая строка"; 
				break;
			}
			case Msg.UNITS: {
				_output.text = "Ошибка. После единиц не может быть слова"; 
				break;
			}
			case Msg.ELEVEN_NINETEEN: {
				_output.text = "Ошибка. После чисел 11-19 не может быть слова"; 
				break;
			}
			
			
				
		}
	}


	private int GetUnits(string sample) {
		int i = 0;
		foreach (var e in unitsList) {
			if (e.Equals(sample)) {
				return i;
			}
			i++;
		}
		
		return -1;
	}

	private int GetTens(string sample) {
		int i = 0;
		foreach (var e in tensList) {
			if (e.Equals(sample)) {
				return (10 + i * 10);
			}
			i++;
		}
		
		return -1;
	}

	private int GetEleven_Nineteen(string sample) {
		int i = 0;
		foreach (var e in eleven_ninteenList) {
			if (e.Equals(sample)) {
				return (11 + i);
			}
			i++;
		}

		return -1;
	}

	private String GetInput() {
		return _inputField.text;
	}
}
