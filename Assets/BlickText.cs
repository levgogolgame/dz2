﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlickText : MonoBehaviour {

	private Shadow _shadow;

	public int speed;

	private bool isRight;
	
	void Start () {
		_shadow = gameObject.GetComponent<Shadow>();

		isRight = false;
	}
	

	void Update () {
		if (_shadow.effectColor.a < 1 )
			isRight = true;
		if (_shadow.effectColor.a  > 254 )
			isRight = false;
		_shadow.effectColor += isRight ? new Color(0, 0, 0, -speed) : new Color(0, 0, 0, speed);
	}
}
