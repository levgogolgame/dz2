﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTextRight : MonoBehaviour {

	public float speed = 2;
	private bool isRight = false;
	private RectTransform _rectTransform;

	// Use this for initialization
	void Start () {
		_rectTransform = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (_rectTransform.localPosition.x < -20 )
			isRight = true;
		if (_rectTransform.localPosition.x > 60 )
			isRight = false;
		_rectTransform.localPosition += (isRight ? Vector3.right : Vector3.left) * speed;

	}
}
