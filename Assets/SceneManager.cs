﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour {

	[SerializeField] private InputField _inputField1;
	[SerializeField] private InputField _inputField2;
	[SerializeField] private Text _output;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Submit")) {
			Submit();
		}
	}

	public void Submit() {
		if (CheckInput(_inputField1.text, _inputField2.text)) {
			
		}
	}

	private bool CheckInput(string text1, string text2) {
		
		try {
			Int32.Parse(text1);
			return true;
		}
		catch (FormatException) {
			Console.WriteLine("Ошибка в первом числе");
			return false;
		}   
		catch (OverflowException) {
			Console.WriteLine("Ошибка в первом числе. Переполнение");
			return false;
		} 
		
		
	}
}
